from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    
    # url(r'^$', 'django_project.views.home', name='home'),
    # url(r'^django_project/', include('django_project.foo.urls')),

    (r'^staff_requests/', include('staffRequests.urls')),
    (r'^notification/', include('notification.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$', 'django_project.views.login'),
    url(r'^accounts/auth_view/$', 'django_project.views.auth_view'),
    url(r'^accounts/invalid_login/$', 'django_project.views.invalid_login'),
    url(r'^accounts/logout/$', 'django_project.views.logout'),
    url(r'^accounts/loggedin/$', 'django_project.views.loggedin'),
    url(r'^accounts/register/$', 'django_project.views.register'),
    url(r'^accounts/register_success/$', 'django_project.views.register_success'),

)
