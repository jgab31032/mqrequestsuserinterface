from django.shortcuts import render_to_response 
from django.http import HttpResponseRedirect, HttpResponse 
from django.contrib import auth 
from django.core.context_processors import csrf 
from django.core.mail import send_mail 
from forms import LoginForm, MyRegistrationForm
from notification.models import Notification 
import logging

def login(request):
	args = {}
	args['form'] = LoginForm()
	args.update(csrf(request))
	return render_to_response('login.html', args)

def auth_view(request):
	username = request.POST.get('username', 'null')
	password = request.POST.get('password', 'null') 
	user = auth.authenticate(username=username, password=password)
	if user is not None:
		auth.login(request, user)
		return HttpResponseRedirect('/accounts/loggedin')
	else:
		return HttpResponseRedirect('/accounts/invalid_login')


def invalid_login(request):
	return render_to_response('invalid_login.html')

def loggedin(request):
	n = Notification.objects.filter(user=request.user, viewed=False)
	args = {}
	args['username'] = request.user.username 
	args['notifications'] = n
	return render_to_response('loggedin.html', args)

def logout(request):
	auth.logout(request)
	return render_to_response('logout.html')

def register(request):
	if request.POST:
		form = MyRegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/accounts/register_success')
	args = {}
	args.update(csrf(request))
	args['form'] = MyRegistrationForm()
	return render_to_response('register.html', args)
		

def register_success(request):
	args = {}
	args['user'] = request.user 
	return render_to_response('register_success.html', args)
