from django.contrib import admin  
from units.models import Unit, Faculty, Department, UnitGroup, Unit, Requisite 



class RequisitesInline(admin.TabularInline):
	model = Requisite 
	extra = 0

class UnitAdmin(admin.ModelAdmin):
	inlines = [RequisitesInline,]

admin.site.register(Unit, UnitAdmin)