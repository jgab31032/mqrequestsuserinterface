from django.db import models
from django.contrib.auth.models import User 


class Faculty(models.Model):
	faculty_name=models.CharField(max_length=50)
	faculty_manager=models.CharField(max_length=50)

class Department(models.Model):
	department_name = models.CharField(max_length=50)
	department_manager = models.CharField(max_length=50) 
	faculty = models.ForeignKey(Faculty, blank=True, null=True)

class UnitGroup(models.Model):
	unitgroup_code = models.IntegerField()
	unit_group_name = models.CharField(max_length=100)
	unit_group_level = models.IntegerField()
	unit_group_manager = models.ForeignKey(User)
	department = models.ForeignKey(Department, blank=True, null=True)

	def __unicode__(self):
		return str(self.program_name)


class Unit(models.Model):
	program = models.ForeignKey(UnitGroup, blank=True, null=True)
	unit_code = models.IntegerField(max_length=10)
	unit_name = models.CharField(max_length=20)
	unit_level = models.IntegerField()
	unit_manager = models.ForeignKey(User)
	unit_description = models.TextField()
	unit_credit_points = models.IntegerField(default=3)

	def __unicode__(self):
		return str(self.unit_name)

class Requisite(models.Model):
	co_requsite = models.BooleanField()
	unit = models.ForeignKey(Unit)
