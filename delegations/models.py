from django.db import models
from django.contrib.auth.models import User 

class Delegate(models.Model):
	APPROVAL_LEVEL_CHOICES = (
		('', ''),
		(1, 'Request arrived in Science Student Centre'),
		(2, 'Request sent to Department'),
		(3, 'Request decided and sent back to Faculty for Action'),
		(4, 'Request finalised'),
		)
	user = models.ForeignKey(User)
	level_delegate_can_approve = models.IntegerField(default=0, choices=APPROVAL_LEVEL_CHOICES)

	def __unicode__(self):
		return str(self.user.username)

class RequestTypeForDelegate(models.Model):
	REQUEST_TYPE_CHOICES = (
		('TT', 'Timetable'),
		('ICC', 'Individual Case Request'),
		('DC', 'Disciplinary Committee'),
		)
	delegate = models.ForeignKey(Delegate)
	request_type = models.CharField(max_length=10, choices=REQUEST_TYPE_CHOICES)

	def __unicode__(self):
		return self.request_type

class EducationalPackage(models.Model):
	EDUC_TYPE_CHOICES = (
		('UN', 'Unit'),
		('CO', 'Course'),
		('ST', 'Student'),
	)
	request_type_for_delegate = models.ForeignKey(Delegate)
	educ_type = models.CharField(max_length=2, choices= EDUC_TYPE_CHOICES)
	educ_name = models.CharField(max_length=10)
	educ_code = models.CharField(max_length=20)

	def __unicode__(self):
		return str(self.educ_type) + str(self.educ_name) + str(self.educ_code)



