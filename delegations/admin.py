from django.contrib import admin
from delegations.models import Delegate, RequestTypeForDelegate, EducationalPackage

class RequestTypeForDelegateInline(admin.TabularInline):
	model = RequestTypeForDelegate
	extra = 0

class EducationalPackageInline(admin.TabularInline):
	model = EducationalPackage
	extra = 0

class DelegateAdmin(admin.ModelAdmin):
	inlines = [RequestTypeForDelegateInline, EducationalPackageInline]


admin.site.register(Delegate, DelegateAdmin)