This will have the following processes: 

1) Requests to make a late addition to the timetable
2) Requests for an individual student case
3) Report a breach of academic dishonesty
4) Requests to make a change in a unit
5) Request to add something on a learnign & teaching committee. 

These are all staff bases requests and all based on generic request model, ie so they are all really similar. 

These requests are the big ticket items of what is causing all the work, sort out this stuff, makes life alot easier

The project consists of: 
* A staff_requests app which has all the basic request types
* A delegations apps which manages all the routing and emailing of requests
* A user_profile app (not nesc, just for fun...)
* A notification app to let users know anything important when they log in
* A units app, (again just for fun, if there was access to the MQ Handbook API) this would allow alot of prepopulation of the forms

Main ToDo at the moment: 
* write unit tests
* make the delegations app more robust
* not much else - the forms are mostly sorted. 