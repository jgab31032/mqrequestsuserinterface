from celery import task 
from django.core.mail import send_mail




@task()
def send_email_to_submitter(email_address):
	send_mail('Submitter',
				'Message',
				'jgab3103@gmail.com',
				[email_address])

@task()
def send_email_to_handler(email_address):
	send_mail('Test from server',
				'Message',
				'jgab3103@gmail.com',
				[email_address])



