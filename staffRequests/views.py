from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.core.context_processors import csrf 
from staffRequests.models import Generalrequest, Requestnote
from staffRequests.models import Timetablerequest, Curricchangerequest
from user_profile.models import UnitApprover
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User 
from units.models import Unit
from staff_requests_functions import create_general_request, notify_new_delegate
from staffRequests.forms import Request_Note_Form, Current_Level_Form, Approval_Status_Form, TimeTable_Request_Form, TimeTableActivities_Form, CurricChange_Request_Form
from django.template import RequestContext 
from django.contrib import messages
from django_project import settings
from django.forms.formsets import formset_factory, BaseFormSet
from reportlab.pdfgen import canvas 
from django.db.models import Q



import logging 
logr = logging.getLogger(__name__)


@login_required
def request_pdf(request):
	a = Generalrequest.objects.get(id=1)
	unit_code = 'bob'
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename="testfile.pdf"'

	p = canvas.Canvas(response)

	p.drawString(100, 100, unit_code)
	p.showPage()
	p.save()
	return response

@login_required
def all_requests(request):
	args = {}
	args.update(csrf(request))
	return render_to_response('staffRequests.html', args)
	


@login_required
def submit_request(request):
	return render_to_response('submit_request.html')

@login_required
def request_tt(request, request_id):
	g = Generalrequest.objects.get(id=request_id)
	if request.POST:
		current_level_form = Current_Level_Form(request.POST)
		note_form = Request_Note_Form(request.POST)
		approval_status_form = Approval_Status_Form(request.POST)

		if note_form.is_valid():
			b = note_form.save(commit = False)
			b.user_adding_note = request.user 
			b.general_request = g
			b.save()
			print b
			messages.success(request, 'Your note was added')
			return HttpResponseRedirect('/staff_requests/request_tt/%s' % request_id)

		elif current_level_form.is_valid():
			g.current_level = current_level_form.cleaned_data['current_level']
			g.save()
			notify_new_delegate(g)
			messages.success(request, 'The level has been changed')
			return HttpResponseRedirect('/staff_requests/request_tt/%s' % request_id)
		
		elif approval_status_form.is_valid():
			g.approval_status = approval_status_form.cleaned_data['approval_status']
			g.save()
			notify_new_delegate(g)
			messages.success(request, 'The approval status')
			return HttpResponseRedirect('/staff_requests/request_tt/%s' % request_id)
			

	else:
		note_form = Request_Note_Form()
		current_level_form = Current_Level_Form()
		approval_status_form = Approval_Status_Form()

	args = {}
	args.update(csrf(request))
	args['request'] = g
	args['approval_status_form'] = approval_status_form
	args['note_form']= note_form 
	args['current_level_form'] = current_level_form
	args['notes'] = g.requestnote_set.all()
	args['user'] = request.user


	return render_to_response("request_tt.html", args,
							context_instance=RequestContext(request))


@login_required
def delete_note(request, note_id):
	n = Requestnote.objects.get(id=note_id)
	general_request_id = n.general_request.id 
	n.delete()
	messages.add_message(request,
				settings.DELETE_MESSAGE,
				"Your comment was deleted")

	return HttpResponseRedirect("/staff_requests/request_tt/%s" % general_request_id)

@login_required
def create_tt(request):
	class RequiredFormSet(BaseFormSet):
		def __init__(self, *args, **kwargs):
			super(RequiredFormSet, self).__init__(*args, **kwargs)
			for form in self.forms:
				form.empty_permitted = False
	TimeTableActivitiesFormSet = formset_factory(TimeTableActivities_Form, max_num=10, formset=RequiredFormSet)
	if request.POST:
		time_table_request_form = TimeTable_Request_Form(request.POST)
		time_table_activities_formset = TimeTableActivitiesFormSet(request.POST, request.FILES)
		if time_table_request_form.is_valid() and time_table_activities_formset.is_valid():
			timetable_request = time_table_request_form.save(commit=False)
			general_request = create_general_request(request.user)
			timetable_request.general_request = general_request
			timetable_request.save()

			for form in time_table_activities_formset.forms:
				timetable_activity = form.save(commit=False)
				timetable_activity.timetable_request = timetable_request
				timetable_activity.save()
			return HttpResponseRedirect('/staff_requests/all_requests/')
	else:
		time_table_request_form = TimeTable_Request_Form()
		time_table_activities_formset = TimeTableActivitiesFormSet()

	args = {}
	args['time_table_request_form'] = time_table_request_form
	args['time_table_activities_formset'] = TimeTableActivitiesFormSet
	args.update(csrf(request))
	return render_to_response('create_tt.html', args)


@login_required
def create_unit_change(request):
	g = Generalrequest.objects.get(id=1)
	if request.POST:
		form = CurricChange_Request_Form(request.POST)
		if form.is_valid():
			curric_change = form.save(commit=False)
			print form
			curric_change.general_request = g
			curric_change.save()
			return HttpResponseRedirect('/staff_requests/all_requests/')
	else:
		form = CurricChange_Request_Form()
	args = {}
	args.update(csrf(request))
	args['form'] = form
	return render_to_response('create_unit_change.html', args)



@login_required
def search_titles(request):
	if request.POST:
		search_text = request.POST['search_text']
	else:
		search_text = ''
	
	staff_requests = Generalrequest.objects.filter(Q(request_type__contains=search_text) | 
													Q(educ_code__contains=search_text) |
													Q(educ_name__contains=search_text) |
													Q(educ_type__contains=search_text)
													)
	
	args = {}
	args['staff_requests'] = staff_requests

	return render_to_response('ajax_search.html', args)
