from django.db import models
from django.contrib.auth.models import User



class Generalrequest(models.Model):
	CURRENT_LEVEL_CHOICES = (
		('', ''),
		(1, 'Make this a new request again in Science Student Centre'),
		(2, 'Send this request to the Department for action'),
		(3, 'This request is back in the Science Student Centre for action'),
		(4, 'Finalise this request'),
		)
	REQUEST_TYPE_CHOICES = (
		('TT', 'Timetable'),
		('ICC', 'Individual Case Request'),
		('DC', 'Disciplinary Committee'),
	)

	APPROVAL_STATUS_CHOICES = (
		('', ''),
		(1, 'Approve this one'),
		(2, "Don't Approve this one"),
		(3, 'This needs further information - I will pop it in the notes below'),
		)

	request_type = models.CharField(max_length=8, choices=REQUEST_TYPE_CHOICES)
	current_level = models.IntegerField(default=0, choices=CURRENT_LEVEL_CHOICES)
	current_handler = models.ForeignKey(User, related_name="current_handler")
	submitted_by = models.ForeignKey(User, related_name="submitted_by")
	date_submitted = models.DateTimeField(auto_now_add=True)
	approval_status = models.IntegerField(default=0, choices=APPROVAL_STATUS_CHOICES)
	educ_type = models.CharField(max_length=20)
	educ_name = models.CharField(max_length=20)
	educ_code = models.CharField(max_length=10)
	
	def __unicode__(self):
		return 'New Request (' + self.request_type + ') submitted by ' + str(self.submitted_by.username)



class Curricchangerequest(models.Model):
	general_request = models.ForeignKey(Generalrequest)
	year_offered = models.BooleanField()
	semester_offered = models.CharField(max_length=10)
	unit_description = models.TextField()
	staff_contact = models.CharField(max_length=100)
	part_of_people_planet = models.BooleanField()
	preco_nccw_change = models.TextField()
	designation = models.CharField(max_length=10)
	request_reason = models.TextField()
	affected_programs = models.TextField()
	students_currently_enrolled = models.IntegerField(default=0)
	students_contacted = models.BooleanField()

	def __unicode__(self):
		return 'hello'




class Timetablerequest(models.Model):
	UNIT_CHOICES = (
		('MATH101', 'MATH101'),
		('ECON101', 'ECON101'),
		('MUS204', 'MUS204'),
		('STAT120', 'STAT120'),
	)
	
	general_request= models.ForeignKey(Generalrequest)
	unit_code = models.CharField(max_length=8, choices = UNIT_CHOICES)
	semester_offered = models.CharField(max_length=10)
	year_offered = models.IntegerField()
	constraints = models.TextField()


	def __unicode__(self):
		return 'Time table request'



class Timetableactivities(models.Model):
	timetable_request = models.ForeignKey(Timetablerequest)
	activity_type = models.CharField(max_length=40)
	required_capacity = models.IntegerField()
	start_date = models.DateTimeField()
	end_date = models.DateTimeField()
	technical_requirements = models.TextField()
	block_mode = models.BooleanField(default=False)


class Requestnote(models.Model):
	general_request = models.ForeignKey(Generalrequest)
	note_text = models.TextField() 
	date_comment_added = models.DateTimeField(auto_now_add=True)
	user_adding_note = models.ForeignKey(User)
	
	def __unicode__(self):
		return str(self.note_text)

