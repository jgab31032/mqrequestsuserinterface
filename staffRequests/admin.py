from django.contrib import admin
from staffRequests.models import Generalrequest, Timetablerequest, Requestnote, Timetableactivities, Curricchangerequest


class TimeTableActiviesInline(admin.TabularInline):
	model = Timetableactivities
	extra = 0

class TimeTableRequestAdmin(admin.ModelAdmin):
	inlines = [TimeTableActiviesInline]



class RequestNoteInline(admin.TabularInline):
	model = Requestnote
	extra = 0

class TimeTableRequestInline(admin.StackedInline):
	model = Timetablerequest
	extra = 0
	inlines = [TimeTableActiviesInline]

class GeneralRequestAdmin(admin.ModelAdmin):
	inlines = [TimeTableRequestInline, RequestNoteInline]

admin.site.register(Curricchangerequest)
admin.site.register(Timetablerequest, TimeTableRequestAdmin)
admin.site.register(Generalrequest, GeneralRequestAdmin)

