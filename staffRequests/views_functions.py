from staffRequests.models import General_Request

def create_general_request(assigned_to, submitted_by):
	a = General_Request(request_type='TT', 
						current_level=1,
						current_handler=assigned_to,
						submitted_by = submitted_by)

	a.save()
	return a