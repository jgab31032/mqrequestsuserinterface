from staffRequests.models import RequestGeneralInfo

def createGenInfoRecord(fields, req, hand):
	a = RequestGeneralInfo( submittedBy = req,
							currentHandler = hand,
							requestor_first_name=fields.cleaned_data['requestor_first_name'],
							requestor_family_name=fields.cleaned_data['requestor_family_name'],
							requestor_department=fields.cleaned_data['requestor_department'])
	
	print 'done'
	return a