from django import forms
from django.forms import Textarea
from form_utils import forms
from staffRequests.models import Requestnote, Generalrequest, Timetablerequest, Timetableactivities, Curricchangerequest
from django.forms.formsets import formset_factory


class CurricChange_Request_Form(forms.BetterModelForm):
	def __init__(self, *args, **kwargs):
		super(CurricChange_Request_Form, self).__init__(*args, **kwargs)
		self.fields['preco_nccw_change'].label = "Change the prerequisites, co-requesites, or nccw status"	
		self.fields['affected_programs'].label = "Are the programs changing for this unit"	
	class Meta:
		model = Curricchangerequest
		widgets = {
			'unit_description' : Textarea(attrs={'placeholder':'Make sure you include the entire description, along with any changes that need to be made'}),
		}
		fieldsets = [('', 
							{'fields': ['unit_description', 'staff_contact',
										'preco_nccw_change', 'designation'], 
							'legend': 'Change Unit Details',
							'classes': ['fieldsetvisible',]
								}),
                     ('', 
                     		{'fields': ['year_offered', 'semester_offered'],
                     		  'legend':'Change Unit Availability',
                     		  'classes': ['fieldsetvisible',]
                            }),
                      ('', 
                     		{'fields': ['part_of_people_planet', 'affected_programs'],
                     		  'legend':'Change the programs and schedules your unit is offered in',
                     		  'classes': ['fieldsetvisible',]
                            }),
                       
                     ]


class TimeTable_Request_Form(forms.BetterModelForm):
	class Meta:
		model = Timetablerequest
		fields = ['unit_code', 'semester_offered', 'year_offered', 'constraints']



class TimeTableActivities_Form(forms.BetterModelForm):
	class Meta:
		model = Timetableactivities
		fields = [
		'activity_type', 
		'required_capacity', 
		'start_date', 
		'end_date',
		'technical_requirements',
		'block_mode'
		]



class Request_Note_Form(forms.BetterModelForm):
	class Meta:
		model = Requestnote
		fields = ['note_text',]


class Approval_Status_Form(forms.BetterModelForm):
	class Meta:
		model = Generalrequest
		fields = ['approval_status',]

class Current_Level_Form(forms.BetterModelForm):
	class Meta:
		model = Generalrequest
		fields = ['current_level',]
